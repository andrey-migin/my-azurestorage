using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace MyAzureTableStorage
{
    public static class CloudStorageAccountExtensions
    {
        
        public static async IAsyncEnumerable<CloudTable> GetCloudTablesAsync(this CloudStorageAccount account)
        {
            var tableClient = account.CreateCloudTableClient();
                
            TableContinuationToken continuationToken = null;

            do
            {
                var response = await tableClient.ListTablesSegmentedAsync(continuationToken);
                continuationToken = response.ContinuationToken;

                foreach (var table in response.Results)
                    yield return table;

            } while (continuationToken != null);
        }
        
        

        public static async IAsyncEnumerable<T> ExecQueryAsync<T>(this CloudTable table, TableQuery<T> tableQuery, TableRequestOptions tableRequestOptions) where T : ITableEntity, new()
        {
            TableContinuationToken tableContinuationToken = null;
            do
            {
                var queryResponse = await table.ExecuteQuerySegmentedAsync(tableQuery, tableContinuationToken, tableRequestOptions, null);
                tableContinuationToken = queryResponse.ContinuationToken;


                foreach (var entity in queryResponse.Results)
                {
                    yield return entity;
                }
            }
            while (tableContinuationToken != null);
        }
        
    }
}