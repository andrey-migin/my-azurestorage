using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;

namespace MyAzureTableStorage
{
    public interface IAzureTableStorage<T>  where T : ITableEntity, new()
    {
        IAsyncEnumerable<T> GetAsync();
        
        IAsyncEnumerable<T> GetAsync(string partitionKey);
        
        ValueTask<T> GetAsync(string partitionKey, string rowKey);

        ValueTask InsertAsync(T entity);
        
        ValueTask InsertOrReplaceAsync(T entity);
        
        ValueTask<T> ReplaceAsync(string partitionKey, string rowKey, Func<T, T> callback);
        
        ValueTask<T> MergeAsync(string partitionKey, string rowKey, Func<T, T> callback);

        ValueTask BulkInsertOrReplaceAsync(IEnumerable<T> entities, int chunkSize = 90);
        ValueTask BulkInsertAsync(IEnumerable<T> entities, int chunkSize = 90);

        ValueTask BulkDeleteAsync(IEnumerable<T> entities, int chunkSize = 90);

        IAsyncEnumerable<T> ExecQueryAsync(TableQuery<T> query);

        ValueTask DeleteAsync(T entity);
    }
}