using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace MyAzureTableStorage
{
    
    public class AzureTableStorageInMemory<T> :  IAzureTableStorage<T>  where T : ITableEntity, new()
    {
        
        public const int Conflict = 409;
        
        public class PartitionInMemory
        {
            
            private readonly Dictionary<string, T> _partitionItems = new Dictionary<string, T>();

            private Exception GetConflictException(string message)
            {
                var exception = StorageException.TranslateException(new Exception(message),

                    new RequestResult
                    {
                        HttpStatusCode = Conflict
                    }

                );

                throw exception;
            }

            public IEnumerable<T> GetAllItems()
            {
                return _partitionItems.Values;
            }

            public void Insert(T entity)
            {
                if (_partitionItems.ContainsKey(entity.RowKey))
                    throw GetConflictException($"Entity with PartitionKey: {entity.PartitionKey} and RowKey: {entity.RowKey}");
                
                _partitionItems.Add(entity.RowKey, entity);
            }

            public void InsertOrReplace(T entity)
            {
                if (_partitionItems.ContainsKey(entity.RowKey))
                    _partitionItems[entity.RowKey] = entity;
                else
                    _partitionItems.Add(entity.RowKey, entity);
            }
            
            public T Get(string rowKey)
            {
                return _partitionItems.ContainsKey(rowKey) 
                    ? _partitionItems[rowKey] 
                    : default;
            }

            public void Delete(string rowKey)
            {
                if (_partitionItems.ContainsKey(rowKey))
                    _partitionItems.Remove(rowKey);
            }

            public void Replace(T entity)
            {
                _partitionItems[entity.RowKey] = entity;
            }
            
        }
        
        
        private readonly Dictionary<string, PartitionInMemory> _data 
            = new Dictionary<string, PartitionInMemory>();

        private static async IAsyncEnumerable<T> ToAsyncList(IEnumerable<T> data)
        {
            foreach (var entity in data)
                yield return entity; 
        }
        
        
        public IAsyncEnumerable<T> GetAsync()
        {

            var result = new List<T>();
            lock (_data)
            {
                foreach (var partition in _data)
                {
                    result.AddRange(partition.Value.GetAllItems());
                }
            }

            return ToAsyncList(result);

        }

        public IAsyncEnumerable<T> GetAsync(string partitionKey)
        {
            var result = new List<T>();
            lock (_data)
            {
                if (_data.ContainsKey(partitionKey)) 
                    result.AddRange(_data[partitionKey].GetAllItems());
            }

            return ToAsyncList(result);
        }

        public ValueTask<T> GetAsync(string partitionKey, string rowKey)
        {
            lock (_data)
            {
                return !_data.ContainsKey(partitionKey) 
                    ? new ValueTask<T>() 
                    : new ValueTask<T>(_data[partitionKey].Get(rowKey));
            }
        }

        private void Insert(T entity)
        {
            if (!_data.ContainsKey(entity.PartitionKey))
                _data.Add(entity.PartitionKey, new PartitionInMemory());
            
            _data[entity.PartitionKey].Insert(entity);
        }

        public ValueTask InsertAsync(T entity)
        {
            lock (_data)
                Insert(entity);
            
            return new ValueTask();
        }

        public ValueTask InsertOrReplaceAsync(T entity)
        {
            lock (_data)
            {
                if (!_data.ContainsKey(entity.PartitionKey))
                    _data.Add(entity.PartitionKey, new PartitionInMemory());

                _data[entity.PartitionKey].InsertOrReplace(entity);
            }
            
            return new ValueTask();
        }

        public ValueTask<T> ReplaceAsync(string partitionKey, string rowKey, Func<T, T> callback)
        {
            lock (_data)
            {
                if (!_data.ContainsKey(partitionKey))
                    return new ValueTask<T>();

                var entity = _data[partitionKey].Get(rowKey);

                if (entity == null)
                    return new ValueTask<T>();

                entity = callback(entity);

                if (entity != null)
                    _data[partitionKey].Replace(entity);
                
                return new ValueTask<T>(entity);
            }
            

        }

        public ValueTask<T> MergeAsync(string partitionKey, string rowKey, Func<T, T> callback)
        {
            return ReplaceAsync(partitionKey, rowKey, callback);
        }

        public ValueTask BulkInsertOrReplaceAsync(IEnumerable<T> entities, int chunkSize = 90)
        {
            lock (_data)
            {
                foreach (var group in entities.GroupBy(itm => itm.PartitionKey))
                {
                    if (!_data.ContainsKey(group.Key))
                        _data.Add(group.Key, new PartitionInMemory());


                    var partition = _data[group.Key];

                    foreach (var entity in group)
                        partition.InsertOrReplace(entity);
                }
            }
            
            return new ValueTask();
        }

        public ValueTask BulkInsertAsync(IEnumerable<T> entities, int chunkSize = 90)
        {
            lock (_data)
            {
                foreach (var tableEntity in entities)
                    Insert(tableEntity);
            }
            
            return new ValueTask();
        }

        public ValueTask BulkDeleteAsync(IEnumerable<T> entities, int chunkSize = 90)
        {
            lock (_data)
            {
                foreach (var tableEntity in entities)
                    Delete(tableEntity);
            }
            
            return new ValueTask();
        }

        public IAsyncEnumerable<T> ExecQueryAsync(TableQuery<T> query)
        {
            throw new NotImplementedException();
        }

        private void Delete(T entity)
        {
            if (!_data.ContainsKey(entity.PartitionKey))
                return;
            _data[entity.PartitionKey].Delete(entity.RowKey);
        }

        public ValueTask DeleteAsync(T entity)
        {
            lock (_data)
            {
                Delete(entity);
            }
            
            return new ValueTask();
        }
    }
}