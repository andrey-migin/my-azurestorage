﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace MyAzureTableStorage
{
    public class AzureTableStorage<T> : IAzureTableStorage<T> where T:ITableEntity, new()
    {

        private string _currentConnectionString;
        private DateTime _lastConnectionStringCheckMoment;
        
        private readonly Func<string> _getConnectionString;
        private readonly string _tableName;


        private readonly TableRequestOptions _requestOptions = new TableRequestOptions
        {
            MaximumExecutionTime = TimeSpan.FromSeconds(5)
        };

        public AzureTableStorage(Func<string> getConnectionString, string tableName)
        {
            _getConnectionString = getConnectionString;
            _tableName = tableName;
        }

        public AzureTableStorage<T> SetExecutionTimeout(TimeSpan timeSpan)
        {
            _requestOptions.MaximumExecutionTime = timeSpan;
            return this;
        }

        private CloudTable _cloudTable;

        private async Task<CloudTable> CreateTableIfNotExistsAsync()
        {
            var connectionString = _getConnectionString();
            var cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
            _currentConnectionString = connectionString;
            _lastConnectionStringCheckMoment = DateTime.UtcNow;
            
            var cloudTableClient = cloudStorageAccount.CreateCloudTableClient();
            var table = cloudTableClient.GetTableReference(_tableName);
            await table.CreateIfNotExistsAsync();
            _cloudTable = table;
            return _cloudTable;
        }


        private readonly TimeSpan _timeSpanConnStringCheckTimeout = TimeSpan.FromMinutes(1); 

        private ValueTask CheckConnectionStringChangeAsync()
        {
            if (DateTime.UtcNow - _lastConnectionStringCheckMoment > _timeSpanConnStringCheckTimeout)
            {
                var newConnectionString = _getConnectionString();

                if (newConnectionString == _currentConnectionString)
                {
                    _lastConnectionStringCheckMoment = DateTime.UtcNow;
                    return new ValueTask();
                }
                
                return new ValueTask(CreateTableIfNotExistsAsync());
            }
            
            return new ValueTask();
        }
        
        private ValueTask<CloudTable> GetTableAsync()
        {
            return _cloudTable != null 
                ? new ValueTask<CloudTable>(_cloudTable) 
                : new ValueTask<CloudTable>(CreateTableIfNotExistsAsync());
        }
        
        
        public async IAsyncEnumerable<T> GetAsync()
        {
            var query =  new TableQuery<T>();
            
            var table = await GetTableAsync();
            var checkTask = CheckConnectionStringChangeAsync();

            await foreach (var item in table.ExecQueryAsync(query, _requestOptions))
                yield return item;


            await checkTask;
        }

        
        public async IAsyncEnumerable<T> GetAsync(string partitionKey)
        {
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey);
            var query =  new TableQuery<T>().Where(filter);
            
            var table = await GetTableAsync();
            
            var checkTask = CheckConnectionStringChangeAsync();
            
            await foreach (var itm in table.ExecQueryAsync(query, _requestOptions))
                yield return itm;


            await checkTask;
        }

        public async ValueTask<T> GetAsync(string partitionKey, string rowKey)
        {
            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            
            var table = await GetTableAsync();
            
            var checkTask = CheckConnectionStringChangeAsync();
            
            var tableResult = await table.ExecuteAsync(retrieveOperation, _requestOptions, null);
            await checkTask;
            
            return (T)tableResult.Result;
        }

        public async ValueTask InsertAsync(T entity)
        {
            var table = await GetTableAsync();
            var checkTask = CheckConnectionStringChangeAsync();
            await table.ExecuteAsync(TableOperation.Insert(entity), _requestOptions, null);
            await checkTask;
        }

        public async ValueTask InsertOrReplaceAsync(T entity)
        {
            var table = await GetTableAsync();
            var checkTask = CheckConnectionStringChangeAsync();
            await table.ExecuteAsync(TableOperation.InsertOrReplace(entity), _requestOptions, null);
            await checkTask;
        }

        public async ValueTask<T> ReplaceAsync(string partitionKey, string rowKey, Func<T, T> callback)
        {

            var table = await GetTableAsync();
            var checkTask = CheckConnectionStringChangeAsync();
            
            T result = default;
            
            for (var i = 0; i < 5; i++)
                try
                {
                    var entity = await GetAsync(partitionKey, rowKey);
                    if (entity != null)
                    {
                        result = callback(entity);
                        if (result != null)
                            await table.ExecuteAsync(TableOperation.Replace(result), _requestOptions, null);

                        return result;
                    }

                    return default;
                }
                catch (StorageException e)
                {
                    if (e.RequestInformation.HttpStatusCode != 412)
                        throw;
                }
            await checkTask;
            return result;
        }

        public async ValueTask<T> MergeAsync(string partitionKey, string rowKey, Func<T, T> callback)
        {
            var table = await GetTableAsync();
            var checkTask = CheckConnectionStringChangeAsync();
            T result = default;
            
            for (var i = 0; i < 5; i++)
                try
                {
                    var entity = await GetAsync(partitionKey, rowKey);
                    if (entity != null)
                    {
                        result = callback(entity);
                        if (result != null)
                            await table.ExecuteAsync(TableOperation.Replace(result), _requestOptions, null);

                        return result;
                    }

                    return default;
                }
                catch (StorageException e)
                {
                    if (e.RequestInformation.HttpStatusCode != 412)
                        throw;
                }

            await checkTask;
            return result;
        }

        public async ValueTask BulkInsertOrReplaceAsync(IEnumerable<T> entities, int chunkSize)
        {
            var table = await GetTableAsync();
            
            var operationsBatch = new TableBatchOperation();
            var checkTask = CheckConnectionStringChangeAsync();

            foreach (var entity in entities)
            {
                operationsBatch.Add(TableOperation.InsertOrReplace(entity));

                if (operationsBatch.Count >= chunkSize)
                {
                    await table.ExecuteBatchAsync(operationsBatch, _requestOptions, null);
                    operationsBatch = new TableBatchOperation();
                }
            }
            
            if (operationsBatch.Count>0)
                await table.ExecuteBatchAsync(operationsBatch, _requestOptions, null);

            await checkTask;
        }

        public async ValueTask BulkInsertAsync(IEnumerable<T> entities, int chunkSize = 90)
        {
            var table = await GetTableAsync();

            var checkTask = CheckConnectionStringChangeAsync();
            var operationsBatch = new TableBatchOperation();

            foreach (var entity in entities)
            {
                operationsBatch.Add(TableOperation.Insert(entity));

                if (operationsBatch.Count >= chunkSize)
                {
                    await table.ExecuteBatchAsync(operationsBatch, _requestOptions, null);
                    operationsBatch = new TableBatchOperation();
                }

            }

            if (operationsBatch.Count > 0)
                await table.ExecuteBatchAsync(operationsBatch, _requestOptions, null);
            
            await checkTask;
        }

        public async IAsyncEnumerable<T> ExecQueryAsync(TableQuery<T> query)
        {
            var table = await GetTableAsync();
            var checkTask = CheckConnectionStringChangeAsync();
            await foreach (var itm in table.ExecQueryAsync(query, _requestOptions))
                yield return itm;
            await checkTask;
        }

        public async ValueTask DeleteAsync(T entity)
        {
            var table = await GetTableAsync();
            var checkTask = CheckConnectionStringChangeAsync();
            await table.ExecuteAsync(TableOperation.Delete(entity), _requestOptions, null);
            await checkTask;
        }
        
        public async ValueTask BulkDeleteAsync(IEnumerable<T> entities, int chunkSize = 90)
        {
            var table = await GetTableAsync();

            var checkTask = CheckConnectionStringChangeAsync();
            var operationsBatch = new TableBatchOperation();

            foreach (var entity in entities)
            {
                operationsBatch.Add(TableOperation.Delete(entity));

                if (operationsBatch.Count >= chunkSize)
                {
                    await table.ExecuteBatchAsync(operationsBatch, _requestOptions, null);
                    operationsBatch = new TableBatchOperation();
                }

            }

            if (operationsBatch.Count > 0)
                await table.ExecuteBatchAsync(operationsBatch, _requestOptions, null);
            
            await checkTask;
        }
    }
}